#include <iostream>
#include <functional>
#include "objets.h"

using namespace std;

struct Usager {
    string prenom, nom;
    unsigned age;

    // this : ptr sur objet actuel. this est optionnel
    void afficher() {
        cout << prenom << " " << nom << ", " << this->age << " ans" << endl;
    }

    void vieillir(int a) {
        age = age+a;
    }

    void rajeunir(int a) {
        vieillir(-a);
    }
};

void basesDeLobjet() {
    cout << "----" << endl << "Classes et objets " << "----" << endl;

    Usager u1;
    u1.nom = "Martin";
    u1.prenom = "Bob";
    u1.age = 78;
    u1.vieillir(4);
    u1.rajeunir(2);
    //function <void(int)> vieillir2 = bind(&Usager::vieillir, u1, placeholders::_1); // OK
    // Ne fonctionne pas, on ne peut pas faire d'operation avec le placeholder
    //function <void(int)> vieillir3 = bind(&Usager::vieillir, u1,vieillir, -(placeholders::_1));
    u1.afficher();
    //cout << "Usager 1 : " << u1.prenom << " " << u1.nom << ", " << u1.age << " ans" << endl;

    // Allocation dynamique (avec un ptr)
    Usager *u2 = new Usager; // Creation dans le tas (ptr + new). Ne pas oublier de delete !!!
    //(*u2).prenom = "Anne"; // *u2 pour recuperer l'element pointe
    //(*u2).nom = "Martin";
    //(*u2).age = 60;
    // les lignes suivantes sont equivalentes a l'ecriture (*u2)
    u2->prenom = "Anne";
    u2->nom = "Martin";
    u2->age = 60;
    u2->afficher();
    // cout <<  "Usager 2 : " << u2->prenom << " " << u2->nom << ", " << u2->age << " ans" << endl;
    delete u2; // a ne surtout pas oublier sinon fuite memoire
}

// Constructeur de Employe (Constructeur vide - sans arguments)
// les attributs sont initialises apres le : pour ne pas laisser les attributs inconnus
// a aucun moment
/*Employe::Employe() : nom("Jane Doe"), id(-1) {
    //nom = "Jane Doe";
    //id = -1;
}*/

//Employe::Employe(string n) : nom(n), id(-1){}
// On peut faire un constructeur qui appelle un autre constructeur de la meme classe
//Employe::Employe(string n) : Employe() {nom = n;}

// Solution optimale de declaration des constructeurs
// Chaque constructeur s'appuie sur un constructeur plus complet
Employe::Employe(string n, int x) : nom(n), id(x) {}
Employe::Employe(string n) : Employe(n, -1){}
Employe::Employe() : Employe("Jane Doe") {}

Employe::~Employe() { cout << "del~ " << endl; }

void Employe::afficher () {
    cout << nom << " (" << id << ")" << endl;
}

void objetsComplets() {
    Employe e1;
    e1.nom = "Michel";
    e1.id = 5;
    e1.afficher();

    // Ne correspond pas a la declaration d'un objet Employe u2 mais au prototype d'une
    // fonction u2() qui renvoie un objet Employe
    // Employe u2(); // => Non !
    Employe e2;
    e2.afficher();

    Employe e3("Didier");
    e3.afficher();

    Employe e4("Donald", 1018);
    e4.afficher();

    // Une autre facon d'initialiser les objets depuis C++ 11 avec {}
    /*Employe e2 {};
    Employe e3{"Didier"};
    Employe e4{"Donald", 1018};
    int n{3};*/
}

// ___________________________________________________________________________________________
//
//      Forme Canonique
// ___________________________________________________________________________________________


// ---------------------------------
// --------- Constructeurs ---------
// ---------------------------------

Equipe::Equipe() : Equipe(0) {}
Equipe::Equipe(unsigned t) : taille(t), employes(new Employe[t]) {}

// Constructeur de recopie
Equipe::Equipe (const Equipe& src) : taille(src.taille), employes(new Employe[src.taille]) {
    // On ne peut pas faire employes = src.employes car reviendrait a partager le meme ptr
    for (int i = 0; i<taille;i++)
        this->employes[i] = src.employes[i];
}

// Constructeur par deplacement
Equipe::Equipe (Equipe && src) : taille(src.taille), employes(src.employes){
    src.employes = nullptr;
}

// ---------------------------------
// ---------- Destructeur ----------
// ---------------------------------

Equipe ::~Equipe() {delete [] employes;}

// -----------------------------------
// --------- Initialistation ---------
// -----------------------------------
void Equipe::initEquipe(){
    for (int i = 0; i<taille;i++)
        // Pb : un objet Employe est cree et detruit a chaque initialisation d'employes[i]
        employes[i] = Employe{};
}

// ---------------------------------
// ----------- Affichage -----------
// ---------------------------------

void Equipe::afficher(){
    for (int i = 0; i<taille;i++) {
        cout << " - ";
        employes[i].afficher();
    }
}

// ---------------------------------
// ---------- Operateur = ----------
// ---------------------------------

// Operateur d'affectation
Equipe& Equipe::operator=(const Equipe& src) {
    // Evite de faire une affectation de type eq1 = eq1
    if (this != &src) {
        this->taille = src.taille;
        // POur eviter une fuite memoire, on supprime le tableau existant d'employes avant
        // d'en creer un nouveau
        delete [] this->employes;
        this->employes = new Employe[src.taille];
        for (int i = 0; i<taille;i++)
            this->employes[i] = src.employes[i];
    }
    return *this;
}

Equipe& Equipe::operator=(Equipe&& src) {
    if (this != &src) {
        this->taille = src.taille;
        this->employes = src.employes;
        src.employes = nullptr;
    }
    return *this;
}

// ----------------------------------
// --------- getLEquipeAmoi ---------
// ----------------------------------

Equipe getLEquipeAMoi() {
    Equipe eq{1};
    eq.employes[0].nom = "Moi";
    eq.employes[0].id = 2000;
    return eq;
}

// ----------------------------------
// --------- formeCanonique ---------
// ----------------------------------
void formeCanonique() {
    Equipe eq1{3};
    //Employe e1{"Marion",1};
    //eq1.employes[0]=e1;
    //Employe e2{"Matt",2};
    //eq1.employes[1]= e2;
    //Employe e3{"Max",3};
    //eq1.employes[2]=e3;
    eq1.initEquipe();
    eq1.afficher();

    Equipe eq2 = eq1; // constructeur par recopie
    Equipe eq3 {eq1}; // constructeur par recopie
    Equipe eq4;
    eq4 = eq1; // affectation, operateur =

    //eq4.afficher();
    eq1.employes[0].id=4001;
    eq3.afficher(); // eq3 et eq1 sont totalement independants

    // Ici, le constructeur par deplacement est appele car getLEquipeAMoi est temporaire
    Equipe eq5 {getLEquipeAMoi()};
    eq5.afficher();
    eq5 = getLEquipeAMoi(); // Operateur = avec ref a droite
}
