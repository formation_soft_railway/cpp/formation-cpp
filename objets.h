#ifndef OBJETS_H_INCLUDED
#define OBJETS_H_INCLUDED

//using namespace std;

void basesDeLobjet();
void objetsComplets();
void formeCanonique();

struct Employe {
    std::string nom;
    int id;

    // Constructeur de la classe. Pas de retour. Meme nom que la clase
    Employe ();
    Employe(std::string);
    Employe(std::string,int);

    //Destructeur
    ~Employe();

    void afficher();
};

// forme canonique
//      - ctor()     => constructeur par defaut
//      - dtor()     => destructeur
//      - ctor(&src) => constructeur de recopie
//      - ctor(&&src) => constructeur de deplacement
//      - op=($src)  => operateur d'affectation
struct Equipe {
    Employe *employes;
    unsigned taille;

    Equipe();
    Equipe(unsigned);
    Equipe(const Equipe&); // COnstructeur de recopie
    Equipe (Equipe&&); //COnstructeur avec ref a droite - Constructeur par deplacement
    ~Equipe();

    void initEquipe();
    void afficher();
    Equipe& operator=(const Equipe&); // Operateur d'affectation
    Equipe& operator=(Equipe&&);

};
#endif // OBJETS_H_INCLUDED
