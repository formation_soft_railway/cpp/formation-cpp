#ifndef OPERATEURS_H_INCLUDED
#define OPERATEURS_H_INCLUDED

using namespace std;

void visibiliteEtOperateurs();

// ---------------------------
// |        Rayon            |
// ---------------------------
// | - nom                   |
// | - ouvrages              |
// ---------------------------
// | + Rayon()               |
// | + Rayon (n,o)           |
// | + afficher()            |
// ---------------------------

class Rayon {
// /!\ Par defaut, les elements d'une classe sont prives !! Pour les rendre visibles hors de
// la classe, il faut ajouter le mot cle "public:"

public:
    Rayon();
    Rayon(const string, const unsigned);
    Rayon (const Rayon&);
    Rayon& operator= (const Rayon&);
    void afficher() const;
    Rayon operator+(const Rayon&)const;
    bool operator==(const Rayon&)const;
    bool operator!=(const Rayon &) const;
    void operator()()const;

private:
    string nom;
    unsigned ouvrages;

    // Une fonction amie a le droit d'acceder aux elements private de la classe
    // L'amitie n'est pas transitive, i.e si une classe A est amie d'une classe B, les amis
    // de la classe B ne sont pas amis de la classe A
    friend void visibiliteEtOperateurs();
};

#endif // OPERATEURS_H_INCLUDED
