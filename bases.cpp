#include <iostream>
#include "bases.h"

using namespace std;

void typesEtTestsEtBoubles()
{
    cout << "Mediatheque de Toulouse :" << endl;

    //int ouvrage = 254000;
    // Les deux declarations suivantes sont equivalentes
    //signed ouvrage = 254000;
    signed int ouvrages = 254'000; // ' est utilis� comme s�parateur, ici ouvrages = 254000
    unsigned dvds = 2890;

    int caisses = 9;
    // int caisses = 0b1001; // �criture binaire
    // int caisses = 011; // �criture octale
    // int caisses = 0x9; // �criture hexad�cimale

    // Un signed int peut stocker 2^31 valeurs (~2'000'000'000)
    cout << "Taille d'un signed int : " << sizeof(ouvrages) << "o" << endl; //4 octets

    // Char : 1 octet (toujours)
    char antennes = 8;
    cout << "Taille d'un char : " << sizeof(char) << "o" << endl; //1 octet

    // Short : >= 2octets
    short rayons = 460;
    cout << "Taille d'un short : " << sizeof(rayons) << "o" << endl; //2 octets

    // Int : 4 octets
    int cds = 82'300;
    cout << "Taille d'un int : " << sizeof(cds) << "o" << endl; //4 octets

    // Long : >= 8 octets
    long emprunts = 123'200;
    cout << "Taille d'un long : " << sizeof(long) << "o" << endl; //4 octets

    // Long Long : 8 octets
    long long entrees = 342'023;
    cout << "Taille d'un long long : " << sizeof(entrees) << "o" << endl; //8 octets

    int16_t auteurs = 1219;
    int16_t max_auteurs = INT16_MAX; // constante permettant de connaitre valeur max possible

    float emprunts_par_jour = 4211.8f; //
    double emprunts_par_an = 322123.1; // 15 chiffres significatifs
    double emprunts_par_an2 = 3.22123e5;

    bool ouverte = true;
    string nom = "Mediatheque rose";

    rayons  = ((rayons * 100)%43) >> 4;
    // & ~ and (ADA) vs. && ~ and then (ADA)
    bool assez_de_rayons = (rayons > 100) && (rayons < 500);
    // || (teste 2�me expression si 1�re fausse) : v. optimis�e de | (teste les deux expressions)
    bool  cd_anormaux = (cds < 0) || (cds > 1'000'000);
    bool cd_normaux = !cd_anormaux; // NOT
    // ^ => OU exclusif
    bool un_nombre_rond = (cds == 10'000) ^ (ouvrages == 100'000);
    cds =  cds +1;
    cds += 1;
    ++cds;
    cds++;
    // int nb_cds_par_bacs = taille_bac / 11;
    double nb_cds_par_bacs_dbl = taille_bac / 10.999;
    // int nb_cds_par_bacs = (int) nb_cds_par_bacs_dbl; // transtypage du C
    int nb_cds_par_bacs = int(nb_cds_par_bacs_dbl); // conversion du C++
    // + conversions de RTTI
    cout << "cds par bacs : " << nb_cds_par_bacs << endl;
    int bacs_cd_necessaires = (cds + nb_cds_par_bacs - 1) / nb_cds_par_bacs;
    cout << "Bacs de CD de 87 cm a acheter en tout : " << bacs_cd_necessaires << endl;
    int espace_restant = nb_cds_par_bacs * bacs_cd_necessaires - cds;
    cout << "Espace restant dans le dernier bac : " << espace_restant << " cds." << endl;

    // ---------------------------- Instructions de tests ----------------------------
    if (espace_restant > 10){
        cout << "On peut aussi mettre les 5 cassettes audios" << endl;
    } else // si une seule ligne dans if / else, {} inutiles
        cout << "On ne peut pas mettre les 5 cassiettes audio" << endl;

    int camion_de_bac = bacs_cd_necessaires /260;
    switch (bacs_cd_necessaires) {
    case 0:
        cout << "on d�m�nage tout en voiture" << endl;
        break; // sans le "break", tous les "case" suivant sont execut�s
    // "case 1, 2:" est impossible. Pour y rem�dier, on peut faire comme suit :
    case 1:
    case 2:
        cout << "un seul camion" << endl;
        break;
    default:
        cout << "plusieurs camions" << endl;
    }

    // A partir du nombre de cds, afficher l'un des messages suivants
    if (cds < 50'000)
        cout << "Moins de 50 000 CDs" << endl;
    else if (cds < 150'000)
        cout << "50 000 a 150 000 CDs" << endl;
    else if (cds < 300'000)
        cout << "150 000 a 300 000 CDs" << endl;
    else
        cout << "Plus de 300 000 CDs" << endl;

    // Exemple avec bloc switch
    /*switch (cds/50'000){
    case 0:
        cout << "Moins de 50 000 CDs" << endl;
        break;
    case 1:
    case 2:
        cout << "50 000 a 150 000 CDs" << endl;
        break;
    case 3:
    case 4:
    case 5:
        cout << "150 000 a 300 000 CDs" << endl;
        break;
    default:
        cout << "Plus de 300 000 CDs" << endl;*/

    // ---------------------------- Instructions de boucle ----------------------------

    unsigned d = 0;
    while (d < bacs_cd_necessaires) {
        cout << "| Bac CD num. " << d << "! |" << endl;
        d += 100;
    }

    d = 0;
    // d = 0 - 200 renvoit la + grande valeur int, une seule boucle sera r�alis�e ici
    do {
        cout << "# Bac CD num. " << d << "! #" << endl;
        d -= 200;
    } while (d < bacs_cd_necessaires);

    for (d = 0; d<bacs_cd_necessaires; d += 300){
        cout << "[Bac CD num. " << d << "! ]" << endl;
    }

    // espace restant = 14
    for (int i=0, j=0; i<espace_restant ; i+=j, j++)
        cout << "Cassette : " << i << endl;
            // 6 tours de boucle
            // i = 0  - j = 0
            // i = 0  - j = 1
            // i = 1  - j = 2
            // i = 3  - j = 3
            // i = 6  - j = 4
            // i = 10 - j = 5

    // ----------------------------------- Tableaux -----------------------------------
    float usagers_par_jour[7]; //ok
    // float usagers_par_jour[3+2*2]; //ok
    // int jours = 7;
    // float usagers_par_jour[jours]; // nok => tableau d�fini qu'avec des constantes
    // const int JOURS = 7;
    // float usagers_par_jour[JOURS]; // ok
    // float usagers_par_jour[JOURS*2-7] // ok
    usagers_par_jour[0] = 125.98f;
    // usagers_par_jour[7] = 90; // pas d'erreur de compilation ou exception declenchee.
                              // Depassement de m�moire : la nlle valeur est �crite � 1
                              // autre adresse, au risque d'�craser une valeur existante
    float usagers_weekend [] {320.5f, 0}; // possible depuis C++ 11

    // Tableau multi-dimension
    float usagers_par_demi_jour[7][2]; // 14 cases * sizeof(float)(=4o) ==> 56o
    usagers_par_demi_jour[6][1] = 0; // dimanche aprem*

    for (float uj : usagers_par_jour)
        cout << "usagers par jour : " << uj << endl;

    int livres_par_rayon [] {2300, 1432, 5602, 976};

    int plus_petit_rayon = 0;
    int plus_grand_rayon = 0;
    //int min_ = livres_par_rayon[0];
    //int max_ = livres_par_rayon[0];

    for (int i = 0; i<4;i++){
        //if (livres_par_rayon[i] > max_){
        if (livres_par_rayon[i] > livres_par_rayon[plus_grand_rayon]){
            //max_ = livres_par_rayon[i];
            plus_grand_rayon = i;
        }
        //if (livres_par_rayon[i] < min_){
        if (livres_par_rayon[i] < livres_par_rayon[plus_petit_rayon]){
            //min_ = livres_par_rayon[i];
            plus_petit_rayon = i;
        }
    }
    cout << "Rayons avec le moins de livres : " << (plus_petit_rayon+1) << endl;
    cout << "Rayons avec le plus de livres : " << (plus_grand_rayon+1) << endl;
}
