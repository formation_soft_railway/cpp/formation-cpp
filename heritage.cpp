#include <iostream>
#include "heritage.h"

using namespace std;

// ___________________________________________________________________________________________
//
//      Classe Affichable
// ___________________________________________________________________________________________

// Affichable est une classe abstraite car contient une methode purement virtuelle.
// on ne peut pas instancie/creer un objet de type Affichable
// une classe abstraite ne peut qu'etre heritee. Une classe abstraite ne peut pas etre final
class Affichable {
public:
    // la fonction est purement virtuelle (a cause du virtual ... =0)
    virtual void afficher()= 0;
};

// ___________________________________________________________________________________________
//
//      Classe Vehicule
// ___________________________________________________________________________________________

// Avec une classe "final", celle-ci ne pourra pas etre heritee par une autre classe
// Ses methodes ne pourront pas etre virtuelles
class Vehicule /*final*/ : public Affichable {
public :
    Vehicule() = default; // force l'implementation du constructeur par defaut
    // empeche l'implementation automatique d'un constructeur par recopie (sans ca, il
    // est automatiqument implemente)
    Vehicule (const Vehicule&) = delete;
    Vehicule (string n) : nom(n) {}

    // virtual permet de faire appel a la classe "reele" plutot que la classe "de base"
    virtual void afficher() {
        cout << nom << " ";
    }

    void afficherJoli() {
        cout << endl << endl;
        cout << "      _____________" << endl ;
        cout << " ____//__][__][___|" << endl ;
        cout << "(o  _|  -|     _ o|" << endl;
        cout << " `-(_)--------(_)-' " << "\t";
        cout << "~ " ;
        afficher();
        cout << " ~";
    }

// Les attributs protected sont accessibles uniquement dans la classe et ses descendants
protected:
    string nom;
    void videNom() {nom = "";}
};

// ___________________________________________________________________________________________
//
//      Classe Camionnette
// ___________________________________________________________________________________________

// Si on fait un heritage "private", tous les attributs et methodes heritees de la classe
// mere seront prives dans la classe fille. En regle generale, la visibilite des elements de
// la classe mere dans la classe fille correspond au plus restricif des 2
// class Camionnette : private Vehicule {
class Camionnette : virtual public Vehicule {
public :
    unsigned livres;
    Camionnette(string n, unsigned l) : Vehicule(n), livres(l) {}

    // "override" n'a pas d'effet a l'execution. Elle peut servir de verificateur/securite
    // qu'on est bien en train de redefinir une fonction de la classe mere (en cas de typo
    // par ex, ou si la fonction de la classe mere est modifiee)

    // "final" rompt la chaine de "virtual" heritee de la classe mere. Si une classe
    //herite de la classe actuelle, la fonction ne sera pas virtuelle pour ses classes filles
    void afficher() /*final*/ override {
        Vehicule::afficher();
        cout << " (" << livres << ") ";
    }
    // Rend publique la fonction protected de la mere
    using Vehicule::videNom;
};

// ___________________________________________________________________________________________
//
//      Classe Bus
// ___________________________________________________________________________________________

class Bus : virtual public Vehicule{
public:
    unsigned accueil;
    Bus(string n, unsigned a) : Vehicule(n), accueil(a) {}

    virtual void afficher() {
        Vehicule::afficher();
        cout << "(max : " << accueil << ") ";
    }
};

// ___________________________________________________________________________________________
//
//      Classe Bibliobus
// ___________________________________________________________________________________________

// Grace a l'heritage virtuelle (voir "virtual" au moment de l'heritage de Vehicule dans les
// classes bus et camionnette), lorsque Bibliobus herite des deux classes, Vehicule n'est
// heritee qu'une fois (au lieu de 2 sans l'heritage vituelle) => heritage en diamant/carreau

//                  Vehicule    Vehicule            Vehicule    Vehicule
//                     \             /                   /         \
//                 Camionnette     Bus      =>     Camionnette     Bus
//                       \         /                     \         /
//                        Bibliobus                       Bibliobus

// [actif [Camionnette [Vehicule]] [Bus [Vehicule]]]
// ==>
// [actif [Camionnette] [Bus] [Vehicule] ]

class Bibliobus : public Camionnette, public Bus {
public:
    bool actif;
    Bibliobus(string n, unsigned l, unsigned acc, bool act):
              Vehicule(n), Camionnette(n,l), Bus(n, acc), actif(act){}

    void afficher() override{
        Camionnette::afficher();
        Bus::afficher();
        if (actif)
            cout << " * actif *";
        else
            cout << " * inactif *";

    }
};
// ___________________________________________________________________________________________
//
//      Heritage simple
// ___________________________________________________________________________________________

void heritageSimple() {
    cout << "----" << endl << "Heritage Simple : " << endl << "----" << endl;

    // Ne fonctionne pas car on a defini un constructeur avec des arguments
    // Fonctionnerait si on n'avait pas defini de constructeur (meme s'il n'y a pas de
    // constructeur par defaut)
    //Vehicule v0;

    Vehicule v1("Peugeot 404");
    v1.afficher();
    v1.afficherJoli();
    cout << endl << endl;

    Camionnette c1("C4",380);
    c1.afficher();
    // early binding : fait appel a la fonction afficher() de la classe mere
    // avec le mot cle virtual => late binding : utilisera afficher () de la classe fille
    c1.afficherJoli();
    cout << endl << endl;
    c1.videNom();

    // Intedit car Affichable est abstraite
    //Affichable a1;

    Bibliobus bb1("Litterature jeunesse", 420, 8, true);
    bb1.afficher();
    cout << endl;

    // Si on appelle la fonction de cette facon, il y a ambiguite. Le compilateur ne sait
    // pas quel methode afficher choisir. Pour forcer, on prefixe le nom de la fonction
    // par le nom de l'une des classes ou la methode est definie
    // avec l'heritage en diamant, pas d'ambiguite
    bb1.afficherJoli();
    //bb1.Camionnette::afficherJoli(); // Non necessaire si heritage en diamant
    // 28, 32, 32, 68 (sans heritage en diamant)
    // 28, 36, 36, 48
    cout << sizeof(Vehicule) << ", " << sizeof (Camionnette) << ", " << sizeof(Bus) << ", " << sizeof(Bibliobus) << endl;


}
