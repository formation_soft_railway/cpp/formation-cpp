#include <iostream>
#include <vector>
#include <array>
#include <list>
#include <deque>
#include <forward_list>
#include <map>
#include <algorithm>
#include <memory>
#include "stl.h"

using namespace std;

// Cette fonction est un predicat
bool assezLong(string s) {return s.size()>=8;}

void collections(){
    cout << "----" << endl << "Collections : " << endl << "----" << endl;

    // ---------------------------------
    // ------------- Vector ------------
    // ---------------------------------
    vector<string> titres {"Illiade", "Odyssee", "Le Retour"};
    titres.push_back("La vengeance"); // ajout a la fin du vecteur
    titres[0] = "L'" + titres[0];
    titres.insert(titres.begin(), "Best of Homere");
    cout << titres.size() << " titres" << endl;

    // afficher tous les titres
    // --------------
    // -- Solution 1 --
    /*for (unsigned i=0; i<titres.size(); i++)
        cout << titres[i] << endl;*/

    // -- Solution 2 --
    /*for (auto it=titres.cbegin(); it!=titres.cend(); it++) // cbegin et cend => constants
    //for (auto it=titres.begin(); it!=titres.end(); it++)
        cout << *it << " ";*/

    // -- Solution 3 --
    for (const string& t : titres)
        cout << t << endl;

    // creer un nouveau vecteur a partir des titres 0, 2, 4, ...
    vector<string> titres2(titres.size()/2);
    // -- Solution 1 --
    /*for (int i=0;i<titres.size();i++) {
        if (i % 2 == 0)
            titres2.push_back(titres[i]);
    }*/

    // -- Solution 2 --
    for (auto it=titres.cbegin(); it<titres.cend(); it += 2)
        titres2.push_back(*it);
    cout << "----------------" << endl;
    /*for (string t : titres2)
    {
        cout << t << endl;
    }*/

    // Autres classes similaires a vector :
    // array : comme un vecteur, mais taille fixe constexpr (determine a la compilation)
    array<string,3> titresCourts {"Jo", "Fame", "Si"}; // plus rapide que vector, moins de memoire
    // list :
    // comme un vecteur, mais sans [], chaque element connait son predecesseur et son successeur
    // On ne peut pas acceder rapidement a une element quelconque de la liste
    list<string> auteurs {"Homere", "Proust", "Zola"};
    // deque : comme un vecteur avec push/pop_front/back (semblable a une pile)
    // sert de pile et de file (d'attente par exemple)
    deque<long> retours {23209, 13209, 14933};
    // forward_list : comme une liste mais chaque element ne connait que le suivant
    // (on ne peut pas parcourir de la fin vers le debut)
    forward_list<string> autresAuteurs {"Sun Zu", "Mao"};

    vector<string> titresLongs (titres.size());
    //copy_if(titres.begin(), titres.end(), titresLongs.begin(), assezLong);
    // Utilisation d'une lambda a la place de assezLong
    copy_if(titres.begin(), titres.end(), titresLongs.begin(),
            [](string s){return s.size()>=8;});
    cout << "Titres longs : " << endl;
    for (const string & s : titresLongs)
        cout << s << " - ";
    cout << endl;

    // 0 : commence la recherche au 1er caractere
    // string::npos : aucun resultat
    bool unY = any_of(titres.begin(), titres.end(),
                      [](string s){return s.find('y', 0)!=string::npos;});
    cout << "un Y ? " << unY << endl;

    bool tousCourts = all_of(titres.begin(), titres.end(),
                      [](string s){return s.size()<8;});
    cout << endl << "Tous - de 8 caracteres ? " << tousCourts << endl;
}

// unordered_ / multi / set/map
// exemples : set, multiset, unordered_multimap
// set : ensemble de cl�s
// map : ensemble de (cl�, valeur) (comme dict en Python)
// unordered : l'ordre d'insertion n'est pas garde
// multi : il peut y avoir des doublons
void ensembles() {
    // Les cles ne peuvent etre que des valeurs comparables (pas de flottants !)
    map<unsigned, unsigned> nouveautesParAnnee { {2018, 23}, {2019, 111}, {2020, 34} };
    if (nouveautesParAnnee.count(2018)>0)
        cout << "2018 : " << nouveautesParAnnee[2018] << " nouveautes !" << endl;
    // "Dictionnaire" dont les cles sont les annees et les valeurs sont des tableaux de 12 elements
    map<unsigned, array<unsigned,12>> nouveautesParAnneeEtMois;
    nouveautesParAnneeEtMois[2021][3] = 0; // acces a mars de l'annee 2021

}

void pointeursIntelligents() { // #include <memory>
    cout << "----" << endl << "Smart pointers : " << endl << "----" << endl;

    string* s1 = new string ("Chaine 1");
    cout << *s1 << endl;
    delete s1;

    // Le pointeur sera supprime a la sortie de la fonction sans qu'on ait a faire un delete
    unique_ptr <string> s2(new string ("Bibliotheque centrale"));
    cout << *s2 << endl;
    // La copie d'un unique_ptr est interdite
    //unique_ptr <string> s3 = s2;


}
