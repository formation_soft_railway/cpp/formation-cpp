#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED

#include <iostream>

using namespace std;

void parametres();
//void montreOuvrage (string titre, string auteur = "Inconnu", unsigned pages = 1);
// noms des arguments optionnels dans le .h
void montreOuvrage (const string, const string = "Inconnu", const unsigned = 1);

void variablesFonctions();

#endif // FONCTIONS_H_INCLUDED
