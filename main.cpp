#include <iostream> // <> Cherche directement dans les r�pertoires standards
#include "bases.h" // " " Cherche d'abord dans le r�pertoire en cours
#include "fonctions.h"
#include "pointeurs.h"
#include "objets.h"
#include "operateurs.h"
#include "heritage.h"
#include "exceptions.h"
#include "templates.h"
#include "stl.h"

using namespace std;

// fonction de d�part
int main(){

    cout << "     _________     " << endl;
    cout << "    / ======= \\    " << endl;
    cout << "   / __________\\   " << endl;
    cout << "  | ___________ |  " << endl;
    cout << "  | | -       | |  " << endl;
    cout << "  | |   C++   | |  " << endl;
    cout << "  | |_________| |  " << endl;
    cout << "  \\_____________/  " << endl;
    cout << "  / \"\"\"\"\"\"\"\"\"\"\" \\  " << endl;
    cout << " / ::::::::::::: \\ " << endl;
    cout << "(_________________)" << endl;

    cout << endl << "________________" << endl << endl << " Debut du Main"<< endl
         << "________________" << endl << endl;

    // _______________________________
    //
    // ----------- bases.h -----------
    // _______________________________
    //
    //typesEtTestsEtBoubles();

    // _______________________________
    //
    // --------- fonctions.h ---------
    // _______________________________
    //
    //parametres();
    //variablesFonctions();

    // _______________________________
    //
    // --------- pointeurs.h ---------
    // _______________________________
    //
    //pointeursEtTableaux();
    //tableauxEtFonctions();
    //references();

    // _______________________________
    //
    // ----------- objets.h ----------
    // _______________________________
    //
    //basesDeLobjet();
    //objetsComplets();
    //formeCanonique();

    // _______________________________
    //
    // --------- operateurs.h --------
    // _______________________________
    //
    //visibiliteEtOperateurs();

    // _______________________________
    //
    // ---------- heritage.h ---------
    // _______________________________
    //
    // heritageSimple();

    // _______________________________
    //
    // --------- exceptions.h --------
    // _______________________________
    //
    //exceptions();

    // _______________________________
    //
    // --------- templates.h --------
    // _______________________________
    //
    //templates();

    // _______________________________
    //
    // ------------- stl.h -----------
    // _______________________________
    //
    //collections();
    //ensembles();
    pointeursIntelligents();

    cout << endl << "________________" << endl << endl << " Fin du Main"<< endl
         << "________________" << endl;
}
