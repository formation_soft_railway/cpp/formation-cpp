#include "fonctions.h"
#include <iostream>
#include <functional> // depuis C++ 11

using namespace std;

// La declaration suivante est autorisee. auto prendra le type de ce qui est renvoy� dans le return
// auto getNbLivreRayon (unsigned longueurEnCm, unsigned etageres) {
unsigned getNbLivreRayon (unsigned longueurEnCm, unsigned etageres) {
    unsigned largeurLivre = 3;
    return longueurEnCm * etageres / largeurLivre;
}

// La declaration suivante n'est pas autorisee. Pas d'auto dans les arguments de fonctons !
//unsigned getNbLivreRayon (auto longueurEnCm, unsigned etageres,
unsigned getNbLivreRayon (unsigned longueurEnCm, unsigned etageres,
                          unsigned largeurDunLivre) {
    return longueurEnCm * etageres / largeurDunLivre;
}

// L'utilisation de const interdit la modification des arguments dans la fonction
void montreOuvrage (const string titre, const string auteur, const unsigned pages){
    cout << "** " << titre << " ** de " << auteur << " (" << pages <<" pages)"<< endl;
}

// Fonction variadique
// Le nombre d'arguments est variable
void montreInfosBibli(int nbInfos, ...){
    // Codes
}

void parametres()
{
    cout << "Parametres de fonctions" << endl;
    cout << "4m de long, 4 �tageres : " << getNbLivreRayon(400, 4) << " livres." << endl;
    cout << "4m de long, 4 �tageres, gros livres : " << getNbLivreRayon(400, 4, 7) <<
            " livres." << endl;

    montreOuvrage("Bonjour");
    montreOuvrage("Toto va a la plage", "Noel");
    montreOuvrage("Titi a la montagne", "Lapin", 200);
}

void variablesFonctions()
{
    // f1 contient montreOuvrage (il agit comme un alias)
    //function<void(string,string, unsigned)> f1 = montreOuvrage;
    auto f1 = montreOuvrage; // f1 prend le type d�clar� de l'autre cote du =
    f1("Asterix aux Jeux Olympiques", "Uderzo, Goscinny", 44);

    // f2 est une facade. f2 != f1. f2 est preparee avec des valeurs definies
    function<void()> f2 = bind(montreOuvrage, "Asterix le Gaulois", "Uderzo, Goscinny", 36);
    f2();

    // placeholders::_1 indique que l'un des param�tres sera declare par l'utilisateur a
    // l'appel dans un bind
    function<void(unsigned)> f3 = bind(montreOuvrage, "Asterix", "Uderzo, Goscinny", placeholders::_1);
    f3(42);

    //function<void(string)> f4 = bind(montreOuvrage, placeholders::_1, "Uderzo, Goscinny", 44);
    auto f4 = bind(montreOuvrage, placeholders::_1, "Uderzo, Goscinny", 44);
    f4("Asterix et Cleopatre"); //(44 pages)

    function<void(unsigned, string)> f5 = bind(montreOuvrage, placeholders::_2, "Uderzo, Goscinny", placeholders::_1);
    f5(42, "La Zizanie");

    /*auto x = 5;
    x = "abx"; // Une fois x declare a la ligne precedente, son type est int. On ne peut pas
               // lui assigner une valeur ne correspondant pas � son type */

}
