#include <iostream>
#include "operateurs.h"

using namespace std;

Rayon::Rayon(const string n, const unsigned o) : nom(n), ouvrages(o){}
Rayon::Rayon() : Rayon("Inconnu", 0) {}
Rayon::Rayon(const Rayon& src) : Rayon(src.nom, src.ouvrages){}

Rayon& Rayon::operator=(const Rayon& src) {
    if (this != &src) {
        this->nom = src.nom;
        this->ouvrages = src.ouvrages;
    }
    return *this;
}

// Ici, le 2eme const indique que la fonction ne modifie pas l'objet en cours (this)
Rayon Rayon::operator+(const Rayon& src2) const {
    Rayon rSomme {nom + " / " + src2.nom, ouvrages+src2.ouvrages};
    return rSomme;
}

bool Rayon::operator==(const Rayon& src)const {
    return (nom == src.nom);
}
bool Rayon::operator!=(const Rayon & src) const {
    return !(*this==src);
}

void Rayon::afficher() const {
    cout << "Rayon : " << nom << " (" << ouvrages << " ouvrages)" << endl;
}

void Rayon::operator()()const {
    afficher();
}

void visibiliteEtOperateurs() {
    cout << "----" << endl << "Visibilite et operateurs" << endl << "----" << endl;

    Rayon r1 ("Geographie", 8711);
    r1.ouvrages += 62; // Possible car la fonction actuelle est amie de la classe Rayon
    r1.afficher();

    Rayon r2 {"Histoire", 789};
    // Ici, on ne passe pas par le constructeur par recopie. le resultat est utilise pour initialiser l'objet
    Rayon r3 = r1 + r2;
    r3.afficher();

    cout << "r3 == r1 ? " << (r3==r1) <<endl;
    cout << "r3 == r3 ? " << (r3==r3) <<endl;
    cout << "r3 != r1 ? " << (r3!=r1) <<endl;
    cout << "r1 != r1 ? " << (r1!=r1) <<endl;

    r1();
}
