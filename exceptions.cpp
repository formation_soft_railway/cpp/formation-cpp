#include "exceptions.h"
#include <iostream>

using namespace std; // associe a la Standard Template Library

// noexcept permet d'indiquer que la fonction ne declenche pas d'exceptions
unsigned getBibliothecaires (int visiteurs) noexcept {
    return 1+visiteurs/25;
}

// la fonction ne declenche pas d'exception car :
//      - cin ne declenche pas d'exception quelque soit la valeur entree
//      - le bloc try/catch gere les exceptions a l'interieur
void exceptions() noexcept {
    cout << "----" << endl << "Exceptions : " << endl << "----" << endl;

    int visiteurs;
    cout << "Visiteurs ? ";
    cin >> visiteurs;
    unsigned tables = 14;
    try {
        if (visiteurs == 0)
            // Declenchement d'exception - Si l'exception est declenchee, les lignes suivantes
            // ne sont pas executees et on execute le catch
            throw 0;
        if (visiteurs < 0)
            throw out_of_range("Trop petit"); // out_of_range : classe d'exception standard
        unsigned tablesParVisiteur = tables / visiteurs;
        cout << "Tables par visiteur : " << tablesParVisiteur << endl;

    // les exceptions sont lancees par valeur et attrapees par ref (ne pas les rattraper
    // par valeur, i.e. sans &)
    } catch (int &codeErreur) {
        cout << "Code d'erreur : " << codeErreur << endl;
    } catch (out_of_range &exc) {
        // what() permet de recuperer le message d'erreur
        cout << "Exception - hors limites - : " << exc.what() << endl;
    } catch(...) { // ... : toutes les exceptions sont rattrappees
        cout << "Erreur inattendue" << endl;
    }

    // on verifie ici si la fonction est declare "noexcept"
    if (noexcept(getBibliothecaires(visiteurs)))
        cout << "Bibliothecaires necessaires : " << getBibliothecaires(visiteurs) << endl;
}
