#include <iostream>
#include <vector>
#include "templates.h"

using namespace std;

// ___________________________________________________________________________________________
//
//      Template de constante
// ___________________________________________________________________________________________

// const int CTE = int(35);
// template de constante
template <typename T> const T PI = T(3.1415792653352847);

// ___________________________________________________________________________________________
//
//      Template de fonction
// ___________________________________________________________________________________________

template<typename T> void afficher(T x, T y) {
    cout << x << " et " << y << endl;
}

template <typename T> T getX() {
    cout << "cout x ? ";
    T x;
    cin >> x;
    return x;
}

template <typename T>T somme (T a, T b, T c) {return a+b+c;}

// Ici class et typename sont equivalents
//template <class T1, class T2> T1 mult(T1 a, T2 b) {
// le type de retour est genere apres le return de la fonction
template <class T1, class T2> decltype(auto) mult(T1 a, T2 b) { return a*b;}

// le decltype remplace le type indique par auto. Permet de garder un type fixe meme
// si la fonction pourrait renvoyer plusieurs types differents
template <class T1, class T2> auto mult2(T1 a, T2 b) -> decltype(a*b) {return a*b;}

// Creation d'un namespace
namespace formation_ib {
    // On peut mettre des namespaces dans un namespace
    namespace toulouse {
        // Template sur un entier
        template<int V> unsigned multiplieur(unsigned x) { return x*V; }
    }

    // On ne peut pas mettre le main dans un namespace !
    // Pour pouvoir utiliser utiliser le contenu du namespace, 3 solutions :
    //      - prefixer avec formation_ib::
    //      - ajouter : using namespace formation_ib (voir exemple fonction templates)
    //      - placer l'appel dans le namespace formation_ib (voir exemple fonction templates)
}

// ___________________________________________________________________________________________
//
//      Template de classe
// ___________________________________________________________________________________________

template<typename T> struct Conteneur {
    T v;

    // Il est difficile de mettre une valeur par defaut pour v sans connaitre son type en avance
    // On peut laisser l'attribut dans un etat inconnu ou faire appel a un constructeur par
    // defaut avec v()
    Conteneur() : v() {}
    Conteneur(const T val) : v(val) {}
    // ne fonctionne que pour les types numeriques
    //bool egalApproximatif(T n) {return (n>v-1) && (n<v+1);}
    // T et T2 devront avoir les operateurs >, < et - pour que ca fonctionne
    template <typename T2>bool egalApproximatif(T2 n) {return (n>v-1) && (n<v+1);}
};

// ___________________________________________________________________________________________
//
//      Specialisation
// ___________________________________________________________________________________________


//template <> string mult2 <string, unsigned>(string a, unsigned b) { return "";}

// Cette fonction sera utilisee a la place de la fonction generique si le type est int
template<> void afficher<int>(int x, int y) {
    cout << x << " and " << y << endl;
}

template<> struct Conteneur<string> {
    std::string v;
    Conteneur() : v("Inconnu") {}
    Conteneur(const string val) : v(val) {}
    //template <typename T2>bool egalApproximatif(T2 n) {return (n>v-1) && (n<v+1);}
};

// Dans le cas des classes, on peut faire une specialisation partielle ou complete
//template<typename T1, typename T2> struct S {...};
//template<typename T1, typename T2> struct S<T1, string> {...};
//template<typename T1, typename T2> struct S<int, T2> {...};
//template<typename T1, typename T2> struct S<int, double> {...};

// ___________________________________________________________________________________________
//
//      Fonction templates
// ___________________________________________________________________________________________

//using namespace formation_ib;

//namespace formation_ib {
void templates() {
    cout << "----" << endl << "Templates : " << endl << "----" << endl;

    // v1 est de type vecteur d'entiers
    vector <int> v1;

    // PI fait partie du namespace global donc ::
    float pif = ::PI<float>;
    cout << "float : " << pif << endl;
    cout << "double : " << PI<double> << endl; // double(3.1415792653352847)
    cout << "unsigned : " << PI<unsigned> << endl; // unsigned(3.1415792653352847)
    // le compilateur essaie de faire string(3.1415)
    //    => il n'existe pas de constructeur string(nombre)
    //cout << "string : " << PI<string> << endl; // interdit

    afficher<int>(4,12);
    // Le compilateur regarde de quel type sont les param d'entree et en deduit le type
    afficher("Ann", "Bob");
    // Ne fonctionne pas, on ne specifie pas de type et comme les 2 types sont differents,
    // il ne sait pas lequel choisir
    // afficher(6,8.3);
    // en specifiant le type, le compilateur transforme automatiquement les valeurs dans le bon type
    afficher<double>(6,8.3);

    // Pour une fonction sans parametre d'entree, il faut specifier le type
    // int v = getX(); // ne fonctionne pas !
    //cout << getX<string>() << endl;

    cout << somme(string("a"), string("b"), string("c")) << " easy as " << somme(1,2,3) << endl;
    cout << "4.6*3 = " << mult(4.6,3) << endl;
    cout << "3 * 4.6 = " << mult(3, 4.6) << endl;

    cout << mult2(4.3,7.2) << " " << mult2(6, 3.4) << " " << mult2(3.4, 6) << endl;

    Conteneur<float>c1;
    c1.v = 2.3199f;
    cout << c1.v << endl;

    Conteneur<float> c2(3.4f);
    cout << "Conteneurs : " << c1.v << " " << c2.v << endl;
    cout << c2.egalApproximatif(4.5) << endl;

    Conteneur <string> c3;
    cout << "Conteneur 3 : " << c3.v << endl; // Conteneur 3 : Inconnu"

    // On ne peut mettre qu'une constante dans <>
    cout << "Template sur entier : " << formation_ib::toulouse::multiplieur<4>(5) << endl; // renvoie 20
    }
//}

