Projet C++ : multiples fichiers

Date : 29/03/2021
STD C++ 14
Realise avec Code::Blocks 20.03

Support de cours : http://www.eni-training.com/Client_Net/mediabook.aspx?idR=188246
git intervenant  : https://gitlab.com/MB_IB/formation-cpp-210329
Documentation    : https://fr.cppreference.com/w/Accueil

-- Compilation -- 
> g++ -o test1.exe test1.cpp
> test1.exe
   - Avec le numero de Version a utiliser 
> g++ -o test1.exe -std=c++14 test1.cpp

-- Commandes Git --
git clone https://gitlab.com/formation_soft_railway/formation-cpp.git
cd formation-cpp

git config --global user.name "user name"
git config --global user.email "example@email.com"

git add myFile.txt
git commit -am "New file"
git push