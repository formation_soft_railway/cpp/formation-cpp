#include <iostream>
#include "pointeurs.h"


using namespace std;

void pointeursEtTableaux() {
    cout << "Pointeurs : " << endl;

    unsigned a = 27; // 4o
    unsigned* pa = &a; // pointeur sur un entier
                       // & : permet de r�cup�rer l'adresse de a
    cout << a << ", " << pa << ", " << endl;

    cout << "pa est a l'adresse : " << &pa << endl;
    unsigned ** ppa = &pa;
    cout << "ou en passant par une nouvelle variable : " << ppa << endl;

    cout << "a, pa et ppa : " << a << ", " << pa << " et " << ppa << endl;
    a++;
    cout << "a, pa et ppa : " << a << ", " << pa << " et " << ppa << endl; // a a chang�

    // les d�clarations suivantes sont interdites
    //pa = &4203;
    //pa = &(a+5);

    cout << "Ce qu'il y a a l'adresse pa : " << *pa << endl; // a
    // cout << "Ce qu'il y a a l'adresse a : " << *a << endl; // interdit ! a n'est pas une adresse

    float tf[5] {0, 2, 4, 6, 8};
    float* pf = nullptr;
    if (pf != nullptr)
        cout << "valeur derriere pf : " << *pf << endl;
    // equivaut a : pf = &tf
    // equivaut a : pf = &tf[0]
    pf = tf;

    // Les 4 appels suivants renvoient la meme valeur
    cout << "4eme valeur du tableau : " << tf[3] << endl;
    cout << "4eme valeur du tableau : " << *(pf+3) << endl; // pf est deplace de 3*4o car il
                                                            // s'agit d'un tableau de float
    pf = pf + 3;
    cout << "4eme valeur du tableau : " << *pf << endl;

    // afficher la somme des valeurs du tableau sans utiliser de []
    float somme = 0;
    pf  = tf;
    for (int i = 0; i<5; i++)
    {
        somme += *pf;
        pf += 1;
    }
    cout << "somme = " << somme << endl;

    // Autre solution
    float total = 0;
    for(pf = tf; pf < tf+5; pf ++)
        total += *pf;
    cout << "Total : " << total << endl;
}

// Les 3 declarations suivantes sont equivalentes
//unsigned minAnnee(unsigned annees[6]) {
//unsigned minAnnee(unsigned annees[]) {
unsigned minAnnee(unsigned* annees, unsigned taille) {
    unsigned min = 2100;

    // interdit car taille inconnue : for(unsigned a : annees)
    //for (unsigned a : annees)
    for (unsigned i = 0; i< taille; i++)
        if (annees[i]<min)
            min = annees[i];
    return min;
}


// Utilisation de la pile : �chec
// A la sortie de la fonction, le tableau est supprime de la pile
/*unsigned* anciennetesPile(unsigned* annees, const unsigned taille) {
    unsigned res[taille];
    for (unsigned i = 0; i < taille; i++)
        res[i] = 2021 - annees[i];
    return res;
}*/

// Solution statique
unsigned* anciennetesStatique(unsigned* annees) {
    // Avec le mot cle "static", le tableau est cree dans la zone statique. Il sera
    // accessible partout
    static unsigned res[6];
    for (unsigned i = 0; i < 6; i++)
        res[i] = 2021 - annees[i];
    return res;
}

// Solution tas
unsigned* anciennetesTas(unsigned* annees, const unsigned taille) {
    // Avec le mot cle "new", le tableau est cree dans le tas, il n'est pas supprime a la sortie
    // de la fonction
    unsigned* res = new unsigned[taille];
    for (unsigned i = 0; i < taille; i++)
        res[i] = 2021 - annees[i];
    return res;
}

void tableauxEtFonctions() {
    cout << "Tableaux et fonctions " << endl;
    unsigned antennes[] {1967, 1966, 1982, 2002, 1987, 2004};
    cout << "Ouverture : " << minAnnee(antennes, 6) << endl;
    // unsigned* ans = anciennetesPile(antennes, 6); // crash !
    unsigned* ansStatique = anciennetesStatique(antennes);
    cout << "Solution statique : ";
    for (unsigned i = 0; i<6; i++)
        cout << ansStatique[i] << " - ";

    unsigned* ansTas = anciennetesTas(antennes, 6);
    cout << endl << "Solution tas : ";
    for (unsigned i = 0; i<6; i++)
        cout << ansTas[i] << " - ";
    delete[] ansTas; // Supprime le tableau de la memoire
}

// Passage d'argument par ref
void afficheBlueRays (unsigned& bluerays) {
    bluerays = 10 * unsigned(bluerays/10);
    cout << "** Bluerays : " << bluerays << endl;
}

// Crash ! On renvoie une ref vers un objet temporaire
// unsigned& arrondiBlueRaysA25Pres(unsigned bluerays) {
unsigned& arrondiBlueRaysA25Pres(unsigned& bluerays) {
    bluerays = 25 * unsigned(bluerays / 25);
    return bluerays;
}

unsigned getHeureOuverture() {return 9;}

string mettreEnForme(string& s) {
    string resultat = " - " + s + " - ";
    return resultat;
    // Solution non ideale : modifie le param d'entree
    // s = " - " + s + " - ";
    // return s;
}

// Param d'entree : ref a droite
// valeur a droite est temporaire. On peut le "casser"
string mettreEnForme (string &&s) {
    s = " - " + s + " - "; // Ecrase la valeur prec
    return s;
}

void references() {
    unsigned bluerays = 3245;
    unsigned& br2 = bluerays; // br2 est une reference sur bluerays
    bluerays += 481; // bluerays : lvalue, 481 : rvalue
    // renvoie la valeur de bluerays, br2 fonctionne comme un alias de bluerays
    cout << "Bluerays : " << br2 << endl;
    afficheBlueRays(bluerays);
    // comme bluerays est passe par ref, la modif faite dans la fonction s'applique partout
    cout << "Bluerays : " << br2 << endl;
    // interdit ! comme argument par ref, il faut mettre une variable en appel
    // afficheBlueRays(1000);
    // afficheBlueRays(bluerays + 5) // idem

    // Interdit ! D�s sa cr�ation et a tout instant, une ref doit referencer qq chose
    // unsigned& br3;
    // br3 = bluerays;

    unsigned& br4 = arrondiBlueRaysA25Pres(bluerays);
    cout << "Bluerays (~25) : " << br4 << endl; // 3700
    cout << "Bluerays (~25) : " << bluerays << endl; // 3700

    // h effectue une recopie de la sortie de la fonction. Potentiellement lent si sortie
    // bcp de donnees
    unsigned h = getHeureOuverture();
    // interdit : pas de rvalue dans ref unsigned& h2. La sortie de la fonction dispara�t a
    // la sortie de la fonction
    // unsigned& h2 = getHeureOuverture();

    // h2 est une ref a un ptr sur la sortie de la fonction. ce n'est pas une recopie
    unsigned&& h2= getHeureOuverture(); // rerference a droite

    string titre = "C++ avance (2eme edition)";
    string misEnForme = mettreEnForme(titre); // appelle la 1ere version
    cout << titre << " => " << misEnForme << endl;
    cout << mettreEnForme("La programmation c'est facile") << endl; // appelle la 2eme version
}
